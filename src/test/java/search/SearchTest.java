package search;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class SearchTest {


    @Rule
    public ExpectedException exceptionRule= ExpectedException.none();

    @Test
    public void searchByTitle() throws IOException {
        Search search=new Search();
        List<Map<String,String>> mapList=search.searchByTitle("java");
        mapList.stream().forEach(list->list.forEach((key,value)->System.out.println(key+":"+value)));
    }
//    @Test(expected = IOException.class)
//    public void searchByTitleIOExc() throws IOException {
//        Search search=new Search();
//        List<Map<String,String>> mapList=search.searchByTitle("java");
//        mapList.stream().forEach(list->list.forEach((key,value)->System.out.println(key+":"+value)));
//    }






}
