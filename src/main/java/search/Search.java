package search;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Search {
    private HttpClient httpClient = new HttpClient();
    private JSONObject json;
    private JSONArray jsonArray;

    public Search() {

    }


    public List<Map<String,String>> searchByTitle(String searchTitle) throws IOException {
        String URL="https://api.stackexchange.com/2.2/search?order=desc&sort=activity&intitle=" +
                searchTitle +"&site=stackoverflow";
        this.json = new JSONObject(httpClient.getJsonString(URL));
        this.jsonArray = (JSONArray)this.json.get("items");
        List<Map<String, String>> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); ++i) {
            Map<String, String> map = new HashMap<String, String>();
            JSONObject obj = jsonArray.getJSONObject(i);
            JSONObject owner = (JSONObject) obj.get("owner");
            String title = obj.getString("title");
            map.put("Title", obj.getString("title"));
            map.put("URL", obj.getString("link"));
            map.put("Author Display Name", owner.getString("display_name"));
            list.add(map);
            if (list.size() > Constants.MAX_RECORDS) {
                break;
            }
        }
        return list;
    }
}
