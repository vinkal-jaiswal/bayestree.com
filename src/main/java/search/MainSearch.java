package search;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MainSearch {

    public MainSearch() {
    }
    public static void main(String[] args) throws IOException {
        Search search = new Search();
        do {
            System.out.print("Please enter search query :");
            Scanner scanner = new Scanner(System.in);
            String searchStr = scanner.next();
            List<Map<String,String>> mapList=search.searchByTitle(searchStr);
            mapList.stream().forEach(list->list.forEach((key,value)->System.out.println(key+" : "+value)));
        } while (true);
    }
}
