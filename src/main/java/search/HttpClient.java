package search;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class HttpClient {

    public HttpClient() {
    }

    public String getJsonString(String url) throws IOException {
        CloseableHttpClient client= HttpClientBuilder.create().build();
        HttpUriRequest uriRequest=new HttpGet(url);
        try {
            HttpResponse response=client.execute(uriRequest);
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            return  br.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
